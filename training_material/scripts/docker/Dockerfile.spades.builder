FROM debian:jessie-slim AS builder

RUN mkdir /input /output /work /install
WORKDIR /install
ENV VENV_DIR /install/conda

# update to latest versions
RUN apt-get update -y && \
    apt-get install -y wget bzip2 && \
    wget -q https://repo.continuum.io/miniconda/Miniconda3-4.5.4-Linux-x86_64.sh -O miniconda.sh && \
    chmod 755 miniconda.sh && \
    bash ./miniconda.sh -b -p $VENV_DIR -f && \
    rm miniconda.sh && \
    . $VENV_DIR/bin/activate && \
    conda update -y conda && \
    conda install -c bioconda spades && \
    touch /install/environment.yml && \
    conda env export -n base -f /install/environment.yml && \
    echo ". /install/conda/bin/activate $(head -1 /install/environment.yml | cut -d' ' -f2)" > ~/.bashrc
ENV PATH /install/conda/bin:$PATH

RUN conda clean --all -y && \
    rm -rf /install/conda/pkgs/*/info && \
    find /install -name '*.a' -exec rm {} \;

FROM debian:jessie-slim
COPY --from=builder /install /install
COPY --from=builder /root/.bashrc /root/.bashrc
ENV PATH /install/conda/bin:$PATH
