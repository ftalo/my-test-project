#!/bin/bash

#
# Get app_version from the environment, or set to DEVELOPMENT by default
app_version=${app_version:=DEVELOPMENT}

#
# Use some git magic to get the branch name and commit-id
git_branch=`git branch | egrep '^\*' | awk '{ print $NF }'`
git_commit_id=`git rev-parse HEAD`

echo "Building the container with APP_VERSION=$app_version, GIT_BRANCH=$git_branch and GIT_COMMIT_ID=$git_commit_id"

docker build \
       --build-arg APP_VERSION=$app_version \
       --build-arg GIT_BRANCH=$git_branch \
       --build-arg GIT_COMMIT_ID=$git_commit_id \
       -t metadata:$app_version \
       -f Dockerfile.metadata .

echo " "
echo "Inspecting the container to see the values of APP_VERSION, BRANCH and COMMIT_ID, baked into the image"
docker inspect metadata:$app_version | egrep uk.ac.ebi | head -3

echo " "
echo "Running the container, to see the values set in the runtime environment"
docker run --rm metadata:$app_version env | egrep 'VERSION|BRANCH|COMMIT_ID'
