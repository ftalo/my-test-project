#!/usr/bin/env bash

sudo apt-get update
sudo apt install -y docker.io

sudo snap install kubectl --classic

curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
sudo cp minikube /usr/local/bin && rm minikube

sudo minikube start --vm-driver=none
